##项目功能：
>1.支持获取、增加、修改、删除schema.xml内容配置

>2.支持获取、增加、修改、删除server.xml内容配置

##使用方法：
```
        // 初始化操作类
        MycatZkOperateApi mycatZkOperateApi = new MycatZkOperateApi(new ZookeeperConfiguration("127.0.0.1:2181"));
        // 获取schema.xml中schema标签的操作对象
        SchemaOperateApi schemaOperateApi = mycatZkOperateApi.getSchemaOperateApi();
        // 获取schema.xml中table标签的操作对象
        TableOperateApi tableOperateApi = mycatZkOperateApi.getTableOperateApi();
        // 获取schema.xml中dataNode标签的操作对象
        DataNodeOperateApi dataNodeOperateApi = mycatZkOperateApi.getDataNodeOperateApi();
        // 获取schema.xml中host标签的操作对象
        DataHostOperateApi dataHostOperateApi =  mycatZkOperateApi.getDataHostOperateApi();
        // 获取server.xml中user标签的操作对象
        ServerUserOperateApi serverUserOperateApi = mycatZkOperateApi.getServerUserOperateApi();
        try {
            // 测试操作user标签
            ServerUser serverUser = new ServerUser("user01","pwd123",Arrays.asList("TestDB"));
            serverUserOperateApi.add(serverUser);
            serverUserOperateApi.update(serverUser);
            serverUserOperateApi.delete(serverUser);
            serverUserOperateApi.query();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            // 关闭连接
            mycatZkOperateApi.close();
        }
```