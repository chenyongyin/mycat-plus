import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cyy.api.MycatZkOperateApi;
import com.cyy.api.operate.*;
import com.cyy.config.ZookeeperConfiguration;
import com.cyy.pojo.server.ServerUser;

import java.util.Arrays;
import java.util.List;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: ZkOperateApiTest
 * @ProjectName mycat-plus
 * @Description: 测试类
 * @date 2019/8/7 13:56
 */
public class ZkOperateApiTest {


    public static void scheamOperate(){
//        ZookeeperConfiguration zkConfig = new ZookeeperConfiguration("127.0.0.1:2181");
//        SchemaOperateApi schemaOperateApi = new SchemaOperateApi(zkConfig);
//        Schema schema = new Schema("test_schema");
//        List<Table> tableList = new ArrayList<>();
//        Table table = new Table("test_table");
//        tableList.add(table);
//        schema.setTable(tableList);
//        schemaOperateApi.add(schema);
        // 初始化操作类
        MycatZkOperateApi mycatZkOperateApi = new MycatZkOperateApi(new ZookeeperConfiguration("127.0.0.1:2181"));
        // 获取schema.xml中schema标签的操作对象
        SchemaOperateApi schemaOperateApi = mycatZkOperateApi.getSchemaOperateApi();
        // 获取schema.xml中table标签的操作对象
        TableOperateApi tableOperateApi = mycatZkOperateApi.getTableOperateApi();
        // 获取schema.xml中dataNode标签的操作对象
        DataNodeOperateApi dataNodeOperateApi = mycatZkOperateApi.getDataNodeOperateApi();
        // 获取schema.xml中host标签的操作对象
        DataHostOperateApi dataHostOperateApi =  mycatZkOperateApi.getDataHostOperateApi();
        // 获取server.xml中user标签的操作对象
        ServerUserOperateApi serverUserOperateApi = mycatZkOperateApi.getServerUserOperateApi();
        try {
            // 测试操作user标签
            ServerUser serverUser = new ServerUser("user01","pwd123",Arrays.asList("TestDB"));
            serverUserOperateApi.add(serverUser);
            serverUserOperateApi.update(serverUser);
            serverUserOperateApi.delete(serverUser);
            serverUserOperateApi.query();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            // 关闭连接
            mycatZkOperateApi.close();
        }
    }

    public static void main(String[] args) throws Exception {
        scheamOperate();
//        System.out.println(new String(schemaOperateApi.getZkClient().getData().forPath("/mycat-cluster-1/schema/schema")));
//        System.out.println(JSON.toJSONString(schemaOperateApi.getZkClient().getChildren().forPath("/")));
    }

}
