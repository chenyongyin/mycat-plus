package com.cyy.enums;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: NodeTypeEnum
 * @ProjectName mycat-plus
 * @Description: 类型
 * @date 2019/8/7 15:03
 */
public enum NodeTypeEnum {
    /** schema标签*/
    SCHEMA,
    /** dataNode标签*/
    DATA_NODE,
    /** dataHost标签*/
    DATA_HOST,
    /** 用户标签*/
    USER,
    /** 表规则标签*/
    TABLE_RULE,
    /** 方法标签*/
    FUNCTIION,
}
