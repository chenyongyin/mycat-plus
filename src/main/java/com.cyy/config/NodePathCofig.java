package com.cyy.config;

import com.cyy.enums.NodeTypeEnum;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: NodePathCofig
 * @ProjectName mycat-plus
 * @Description: 节点路径配置
 * @date 2019/8/7 10:20
 */
public final class NodePathCofig {

    private static final String SCHEMA_NODE = "schema/schema";

    private static final String SCHEMA_DATA_NODE = "schema/dataNode";

    private static final String SCHEMA_DATA_HOST_NODE = "schema/dataHost";

    private static final String SERVER_USER_NODE = "server/user";

    private static final String RULES_TABLE_RULE_NODE = "rules/tableRule";

    private static final String RULES_FUNCTION_NODE = "rules/function";

    private String clusterId;

    public NodePathCofig(String clusterId) {
        this.clusterId = clusterId;
    }

    /**
     * @return java.lang.String
     * @author chenyongyin
     * @description 获取路径
     * @date 2019/8/7 15:14
     * @params [nodeTypeEnum]
     **/
    public String getFullPath(NodeTypeEnum nodeTypeEnum) {
        switch (nodeTypeEnum) {
            case SCHEMA:
                return getSchemaNodePath();
            case DATA_NODE:
                return getSchemaDataNodePath();
            case DATA_HOST:
                return getSchemaDataHostNodePath();
            case USER:
                return getServerUserNodePath();
            case TABLE_RULE:
                return getTableRuleNodePath();
            case FUNCTIION:
                return getSchemaNodePath();
            default:
                return getReluesFunctionNodePath();
        }
    }

    public String getClusterPath() {
        return String.format("/%s", clusterId);
    }

    public String getSchemaNodePath() {
        return String.format("/%s/%s", clusterId, SCHEMA_NODE);
    }

    public String getSchemaDataNodePath() {
        return String.format("/%s/%s", clusterId, SCHEMA_DATA_NODE);
    }

    public String getSchemaDataHostNodePath() {
        return String.format("/%s/%s", clusterId, SCHEMA_DATA_HOST_NODE);
    }

    public String getServerUserNodePath() {
        return String.format("/%s/%s", clusterId, SERVER_USER_NODE);
    }

    public String getTableRuleNodePath() {
        return String.format("/%s/%s", clusterId, RULES_TABLE_RULE_NODE);
    }

    public String getReluesFunctionNodePath() {
        return String.format("/%s/%s", clusterId, RULES_FUNCTION_NODE);
    }
}
