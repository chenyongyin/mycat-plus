package com.cyy.api.operate;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cyy.api.AbstractZkOperateApi;
import com.cyy.enums.NodeTypeEnum;
import com.cyy.pojo.schema.DataNode;
import com.cyy.pojo.schema.Schema;
import org.apache.curator.framework.CuratorFramework;

import java.util.List;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: SchemaOperateApi
 * @ProjectName mycat-plus
 * @Description: 操作schema标签
 * @date 2019/8/7 13:25
 */
public class SchemaOperateApi extends AbstractZkOperateApi<Schema> {

     public SchemaOperateApi(CuratorFramework zkClient){
        super.zkClient = zkClient;
    }

    @Override
    public void add(Schema schema) throws Exception {
        eachAdd((JSONObject) JSON.toJSON(schema),NodeTypeEnum.SCHEMA);
    }

    @Override
    public void update(Schema schema) throws Exception {
        eachUpdate((JSONObject) JSON.toJSON(schema),NodeTypeEnum.SCHEMA);
    }

    @Override
    public void delete(Schema schema) throws Exception {
        eachDelete((JSONObject) JSON.toJSON(schema),NodeTypeEnum.SCHEMA);
    }

    @Override
    public List<Schema> query() throws Exception {
        return JSONArray.parseArray(query(NodeTypeEnum.SCHEMA),Schema.class);
    }
}
