package com.cyy.api.operate;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.cyy.api.AbstractZkOperateApi;
import com.cyy.config.NodePathCofig;
import com.cyy.enums.NodeTypeEnum;
import com.cyy.pojo.schema.Table;
import org.apache.curator.framework.CuratorFramework;

import java.util.List;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: TableOperateApi
 * @ProjectName mycat-plus
 * @Description: 操作表标签
 * @date 2019/8/7 16:27
 */
public class TableOperateApi extends AbstractZkOperateApi<Table> {

    public TableOperateApi(CuratorFramework zkClient) {
        super.zkClient = zkClient;
    }

    @Override
    public void add(Table table) throws Exception {
        String schema = table.getSchema();
        List<String> clusterIdList = this.zkClient.getChildren().forPath("/");
        for (String clusterId : clusterIdList) {
            NodePathCofig nodePathCofig = new NodePathCofig(clusterId);
            String path = nodePathCofig.getFullPath(NodeTypeEnum.SCHEMA);
            JSONArray jsonArray;
            String dataStr = new String(zkClient.getData().forPath(path));
            if (dataStr.length() == 0) {
                jsonArray = JSON.parseArray(dataStr);
                for (int i = 0, size = jsonArray.size(); i < size; i++) {
                    // 存在schema才增加
                    if (schema.equals(jsonArray.getJSONObject(i).getString("name"))) {
                        jsonArray.getJSONObject(i).getJSONArray("table").add(table);
                        break;
                    }
                }
                this.zkClient.setData().forPath(path, jsonArray.toJSONString().getBytes());
            }
        }
    }

    @Override
    public void update(Table table) throws Exception {
        String schema = table.getSchema();
        String tableName = table.getName();
        List<String> clusterIdList = this.zkClient.getChildren().forPath("/");
        for (String clusterId : clusterIdList) {
            NodePathCofig nodePathCofig = new NodePathCofig(clusterId);
            String path = nodePathCofig.getFullPath(NodeTypeEnum.SCHEMA);
            JSONArray schemaArray;
            String dataStr = new String(zkClient.getData().forPath(path));
            if (dataStr.length() == 0) {
                schemaArray = JSON.parseArray(dataStr);
                for (int i = 0, size = schemaArray.size(); i < size; i++) {
                    // 存在schema才增加
                    if (schema.equals(schemaArray.getJSONObject(i).getString("name"))) {
                        JSONArray tableArray = schemaArray.getJSONObject(i).getJSONArray("table");
                        if (tableArray != null && !tableArray.isEmpty()) {
                            for (int j = 0, len = tableArray.size(); j < len; j++) {
                                if (tableName.equals(tableArray.getJSONObject(j).getString("name"))) {
                                    tableArray.remove(j);
                                    tableArray.add(table);
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
                this.zkClient.setData().forPath(path, schemaArray.toJSONString().getBytes());
            }
        }
    }

    @Override
    public void delete(Table table) throws Exception {
        String schema = table.getSchema();
        String tableName = table.getName();
        List<String> clusterIdList = this.zkClient.getChildren().forPath("/");
        for (String clusterId : clusterIdList) {
            NodePathCofig nodePathCofig = new NodePathCofig(clusterId);
            String path = nodePathCofig.getFullPath(NodeTypeEnum.SCHEMA);
            JSONArray schemaArray;
            String dataStr = new String(zkClient.getData().forPath(path));
            if (dataStr.length() == 0) {
                schemaArray = JSON.parseArray(dataStr);
                for (int i = 0, size = schemaArray.size(); i < size; i++) {
                    // 存在schema才增加
                    if (schema.equals(schemaArray.getJSONObject(i).getString("name"))) {
                        JSONArray tableArray = schemaArray.getJSONObject(i).getJSONArray("table");
                        if (tableArray != null && !tableArray.isEmpty()) {
                            for (int j = 0, len = tableArray.size(); j < len; j++) {
                                if (tableName.equals(tableArray.getJSONObject(j).getString("name"))) {
                                    tableArray.remove(j);
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
                this.zkClient.setData().forPath(path, schemaArray.toJSONString().getBytes());
            }
        }
    }

    @Override
    public List<Table> query() {
        return null;
    }
}
