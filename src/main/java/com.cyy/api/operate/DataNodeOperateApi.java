package com.cyy.api.operate;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cyy.api.AbstractZkOperateApi;
import com.cyy.enums.NodeTypeEnum;
import com.cyy.pojo.schema.DataNode;
import org.apache.curator.framework.CuratorFramework;

import java.util.List;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: DataNodeOperateApi
 * @ProjectName mycat-plus
 * @Description: dataNode 标签 操作类
 * @date 2019/8/8 9:07
 */
public class DataNodeOperateApi extends AbstractZkOperateApi<DataNode> {
    public DataNodeOperateApi(CuratorFramework zkClient) {
        super.zkClient = zkClient;
    }

    @Override
    public void add(DataNode dataNode) throws Exception {
        eachAdd((JSONObject) JSON.toJSON(dataNode),NodeTypeEnum.DATA_NODE);
    }

    @Override
    public void update(DataNode dataNode) throws Exception {
        eachUpdate((JSONObject) JSON.toJSON(dataNode),NodeTypeEnum.DATA_NODE);
    }

    @Override
    public void delete(DataNode dataNode) throws Exception {
        eachDelete((JSONObject) JSON.toJSON(dataNode),NodeTypeEnum.DATA_NODE);
    }

    @Override
    public List<DataNode> query() throws Exception {
        return JSONArray.parseArray(query(NodeTypeEnum.DATA_NODE),DataNode.class);
    }
}
