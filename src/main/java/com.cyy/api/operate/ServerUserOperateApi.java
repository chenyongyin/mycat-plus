package com.cyy.api.operate;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cyy.api.AbstractZkOperateApi;
import com.cyy.enums.NodeTypeEnum;
import com.cyy.pojo.server.ServerUser;
import org.apache.curator.framework.CuratorFramework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: ServerUserOperateApi
 * @ProjectName mycat-plus
 * @Description: server中用户标签的操作
 * @date 2019/8/8 9:15
 */
public class ServerUserOperateApi extends AbstractZkOperateApi<ServerUser> {

    public ServerUserOperateApi(CuratorFramework zkClient) {
        super.zkClient = zkClient;
    }

    @Override
    public void add(ServerUser serverUser) throws Exception {
         eachAdd(pojo2Json(serverUser),NodeTypeEnum.USER);
    }

    @Override
    public void update(ServerUser serverUser) throws Exception {
        eachUpdate(pojo2Json(serverUser),NodeTypeEnum.USER);
    }

    @Override
    public void delete(ServerUser serverUser) throws Exception {
        eachDelete((JSONObject) JSON.toJSON(serverUser),NodeTypeEnum.USER);
    }

    @Override
    public List<ServerUser> query() throws Exception {
        List<ServerUser> serverUserList = new ArrayList<>();
        String dataStr = query(NodeTypeEnum.USER);
        JSONArray jsonArray = JSON.parseArray(dataStr);
        for(int i = 0,size = jsonArray.size();i<size;i++){
            JSONObject userJson = new JSONObject();
            JSONObject dataJson = jsonArray.getJSONObject(i);
            JSONArray propertyArray = dataJson.getJSONArray("property");
            userJson.put("name",dataJson.getString("name"));
            for(int j = 0,len = propertyArray.size();j<len;j++){
                JSONObject propertyJson = propertyArray.getJSONObject(j);
                String name = propertyJson.getString("name");
                if("schemas".equals(name)){
                    String value = propertyJson.getString("value");
                    String[] valueArray = value.split(",");
                    userJson.put(name,Arrays.asList(valueArray));
                }else{
                    userJson.put(name,propertyJson.get("value"));
                }
            }
            serverUserList.add(userJson.toJavaObject(ServerUser.class));
        }
        return serverUserList;
    }

    /**
     * @author chenyongyin
     * @description 将serverUser对象转换成json
     * @date 2019/8/8 9:35
     * @params [serverUser]
     * @return com.alibaba.fastjson.JSONObject
     **/
    private JSONObject pojo2Json(ServerUser serverUser){
        JSONObject userJson = new JSONObject();
        JSONArray propertyArray = new JSONArray();
        userJson.put("name",serverUser.getName());
        JSONObject userTempJson = (JSONObject) JSON.toJSON(serverUser);
        Set<String> keys = userTempJson.keySet();
        for(String key : keys){
            if("name".equals(key)){
                continue;
            }
            JSONObject propertyJson = new JSONObject();
            Object value = userTempJson.get(key);
            if("schemas".equals(key)){
                value = String.join(",",JSON.parseArray(userTempJson.getString(key),String.class));
            }
            propertyJson.put("name",key);
            propertyJson.put("value",value);
            propertyArray.add(propertyJson);
        }
        userJson.put("property",propertyArray);
        return userJson;
    }
}
