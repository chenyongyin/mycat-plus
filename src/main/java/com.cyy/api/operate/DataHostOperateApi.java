package com.cyy.api.operate;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cyy.api.AbstractZkOperateApi;
import com.cyy.enums.NodeTypeEnum;
import com.cyy.pojo.schema.DataHost;
import org.apache.curator.framework.CuratorFramework;

import java.util.List;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: DataHostOperateApi
 * @ProjectName mycat-plus
 * @Description: dataHost标签操作
 * @date 2019/8/8 11:17
 */
public class DataHostOperateApi extends AbstractZkOperateApi<DataHost> {

    public DataHostOperateApi(CuratorFramework zkClient) {
        super.zkClient = zkClient;
    }

    @Override
    public void add(DataHost dataHost) throws Exception {
        eachAdd((JSONObject) JSON.toJSON(dataHost),NodeTypeEnum.DATA_HOST);
    }

    @Override
    public void update(DataHost dataHost) throws Exception {
        eachUpdate((JSONObject) JSON.toJSON(dataHost),NodeTypeEnum.DATA_HOST);
    }

    @Override
    public void delete(DataHost dataHost) throws Exception {
        eachDelete((JSONObject) JSON.toJSON(dataHost),NodeTypeEnum.DATA_HOST);
    }

    @Override
    public List<DataHost> query() throws Exception {
        return JSONArray.parseArray(query(NodeTypeEnum.DATA_HOST),DataHost.class);
    }
}
