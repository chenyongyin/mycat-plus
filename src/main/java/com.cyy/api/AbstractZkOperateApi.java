package com.cyy.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cyy.config.NodePathCofig;
import com.cyy.enums.NodeTypeEnum;
import org.apache.curator.framework.CuratorFramework;

import java.util.List;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: ZkMycatOperateApi
 * @ProjectName mycat-plus
 * @Description: mycat zk 配置操作类
 * @date 2019/8/7 11:10
 */
public abstract class AbstractZkOperateApi<T> {
    protected CuratorFramework zkClient;

    protected void eachAdd(JSONObject dataObj, NodeTypeEnum nodeTypeEnum) throws Exception {
        List<String> clusterIdList = this.zkClient.getChildren().forPath("/");
        for (String clusterId : clusterIdList) {
            NodePathCofig nodePathCofig = new NodePathCofig(clusterId);
            String path = nodePathCofig.getFullPath(nodeTypeEnum);
            JSONArray jsonArray;
            String dataStr = new String(zkClient.getData().forPath(path));
            if (dataStr.length() != 0) {
                jsonArray = JSON.parseArray(dataStr);
                for (int i = 0, size = jsonArray.size(); i < size; i++) {
                    // 如果已经存在则不进行添加
                    if (dataObj.getString("name").equals(jsonArray.getJSONObject(i).getString("name"))) {
                        return;
                    }
                }
            } else {
                jsonArray = new JSONArray();
            }
            jsonArray.add(dataObj);
            this.zkClient.setData().forPath(path, jsonArray.toJSONString().getBytes());
        }
    }

    protected void eachUpdate(JSONObject dataObj, NodeTypeEnum nodeTypeEnum) throws Exception {
        List<String> clusterIdList = this.zkClient.getChildren().forPath("/");
        for (String clusterId : clusterIdList) {
            NodePathCofig nodePathCofig = new NodePathCofig(clusterId);
            String path = nodePathCofig.getFullPath(nodeTypeEnum);
            JSONArray jsonArray;
            String dataStr = new String(zkClient.getData().forPath(path));
            if (dataStr.length() != 0) {
                jsonArray = JSON.parseArray(dataStr);
                for (int i = 0, size = jsonArray.size(); i < size; i++) {
                    // 寻找到相等的  然后替换内容
                    if (dataObj.getString("name").equals(jsonArray.getJSONObject(i).getString("name"))) {
                        jsonArray.remove(i);
                        jsonArray.add(dataObj);
                        break;
                    }
                }
                this.zkClient.setData().forPath(path, jsonArray.toJSONString().getBytes());
            }
        }
    }

    protected void eachDelete(JSONObject dataObj, NodeTypeEnum nodeTypeEnum) throws Exception {
        List<String> clusterIdList = this.zkClient.getChildren().forPath("/");
        for (String clusterId : clusterIdList) {
            NodePathCofig nodePathCofig = new NodePathCofig(clusterId);
            String path = nodePathCofig.getFullPath(nodeTypeEnum);
            JSONArray jsonArray;
            String dataStr = new String(zkClient.getData().forPath(path));
            if (dataStr.length() != 0) {
                jsonArray = JSON.parseArray(dataStr);
                for (int i = 0, size = jsonArray.size(); i < size; i++) {
                    // 寻找到相等的  然后替换内容
                    if (dataObj.getString("name").equals(jsonArray.getJSONObject(i).getString("name"))) {
                        jsonArray.remove(i);
                        break;
                    }
                }
                this.zkClient.setData().forPath(path, jsonArray.toJSONString().getBytes());
            }
        }
    }

    protected String query(NodeTypeEnum nodeTypeEnum) throws Exception {
        String clusterId = this.zkClient.getChildren().forPath("/").get(0);
        NodePathCofig nodePathCofig = new NodePathCofig(clusterId);
        return new String(zkClient.getData().forPath(nodePathCofig.getFullPath(nodeTypeEnum)));
    }

    public abstract void add(T t) throws Exception;

    public abstract void update(T t) throws Exception;

    public abstract void delete(T t) throws Exception;

    public abstract List<T> query() throws Exception;
}
