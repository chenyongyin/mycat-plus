package com.cyy.api;

import com.cyy.api.operate.*;
import com.cyy.config.ZookeeperConfiguration;
import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import lombok.Getter;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.ACLProvider;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.utils.CloseableUtils;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.data.ACL;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: MycatZkOperateApi
 * @ProjectName mycat-plus
 * @Description: TODO
 * @date 2019/8/7 16:21
 */
public class MycatZkOperateApi {

    @Getter
    public CuratorFramework zkClient;
    @Getter
    private final SchemaOperateApi schemaOperateApi;
    @Getter
    private final TableOperateApi tableOperateApi;
    @Getter
    private final DataNodeOperateApi dataNodeOperateApi;
    @Getter
    private final ServerUserOperateApi serverUserOperateApi;
    @Getter
    private final DataHostOperateApi dataHostOperateApi;

    public MycatZkOperateApi(ZookeeperConfiguration zkConfig){
        init(zkConfig);
        schemaOperateApi = new SchemaOperateApi(this.zkClient);
        tableOperateApi = new TableOperateApi(this.zkClient);
        dataNodeOperateApi = new DataNodeOperateApi(this.zkClient);
        serverUserOperateApi = new ServerUserOperateApi(this.zkClient);
        dataHostOperateApi = new DataHostOperateApi(this.zkClient);
    }

    protected void init(ZookeeperConfiguration zkConfig){
        CuratorFrameworkFactory.Builder builder = CuratorFrameworkFactory.builder()
                .connectString(zkConfig.getServerLists())
                .retryPolicy(new ExponentialBackoffRetry(zkConfig.getBaseSleepTimeMilliseconds(), zkConfig.getMaxRetries(), zkConfig.getMaxSleepTimeMilliseconds()))
                .namespace(zkConfig.getNamespace());
        if (0 != zkConfig.getSessionTimeoutMilliseconds()) {
            builder.sessionTimeoutMs(zkConfig.getSessionTimeoutMilliseconds());
        }
        if (0 != zkConfig.getConnectionTimeoutMilliseconds()) {
            builder.connectionTimeoutMs(zkConfig.getConnectionTimeoutMilliseconds());
        }
        if (!Strings.isNullOrEmpty(zkConfig.getDigest())) {
            builder.authorization("digest", zkConfig.getDigest().getBytes(Charsets.UTF_8))
                    .aclProvider(new ACLProvider() {

                        @Override
                        public List<ACL> getDefaultAcl() {
                            return ZooDefs.Ids.CREATOR_ALL_ACL;
                        }

                        @Override
                        public List<ACL> getAclForPath(final String path) {
                            return ZooDefs.Ids.CREATOR_ALL_ACL;
                        }
                    });
        }
        zkClient = builder.build();
        zkClient.start();
        try {
            if (!zkClient.blockUntilConnected(zkConfig.getMaxSleepTimeMilliseconds() * zkConfig.getMaxRetries(), TimeUnit.MILLISECONDS)) {
                zkClient.close();
                throw new KeeperException.OperationTimeoutException();
            }
            //CHECKSTYLE:OFF
        } catch (final Exception ex) {
            //CHECKSTYLE:ON
            throw new RuntimeException(ex);
        }
    }

    public void close() {
        CloseableUtils.closeQuietly(zkClient);
    }
}
