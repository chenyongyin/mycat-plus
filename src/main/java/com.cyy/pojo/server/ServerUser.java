package com.cyy.pojo.server;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: User
 * @ProjectName mycat-plus
 * @Description: mycat对的用户权限
 * @date 2019/8/7 10:18
 */
@Setter
@Getter
@RequiredArgsConstructor
public class ServerUser {
    /** 登陆用户名*/
    private final String name;
    /** 登陆密码*/
    private final String password;
    /** 逻辑库 要访问TESTDB 必须现在server.xml 中定义，否则无法访问TESTDB**/
    private final List<String> schemas;
    /** 配置是否允许只读*/
    private boolean readOnly;
    /** 定义限制前端整体的连接数，如果其值为0，或者不设置，则表示不限制连接数量*/
    private int benchmark;
    /** 设置是否开启密码加密功能，默认为0不开启加密，为1则表示开启加密*/
    private int usingDecrypt;

}
