package com.cyy.pojo.schema;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: Table
 * @ProjectName mycat-plus
 * @Description: table
 * @date 2019/8/7 13:35
 */
@Setter
@Getter
@RequiredArgsConstructor
public final class Table implements Serializable {
    private final String schema;
    /**
     * 逻辑表的表名，同一个schema表名必须唯一
     */
    private final String name;
    /**
     * 定义这个逻辑表所属的 dataNode，用英文逗号间隔，如：dataNode="dn1,dn2"
     */
    private String dataNode;
    /**
     * 指定该逻辑表对应真实表的主键。MyCat会缓存主键（通过primaryKey属性配置）与具体 dataNode的信息。
     * 当分片规则使用非主键进行分片时，那么在使用主键进行查询时，MyCat就会通过缓存先确定记录在哪个dataNode上，然后再在该dataNode上执行查询
     */
    private String primaryKey;
    /**
     * 用于指定逻辑表要使用的规则名字，规则名字在 rule.xml 中定义，必须与 tableRule 标签中 name 属性属性值一一对应
     */
    private String rule;
    /**
     * 用于指定表是否绑定分片规则，如果配置为 true，但没有配置具体 rule 的话 ，程序会报错
     */
    private String ruleRequired;
    /**
     * 定义了逻辑表的类型，目前逻辑表只有“全局表”和”普通表”两种类型。对应的配置：
     * 全局表：global。
     * 普通表：不指定该值为 global 的所有表。
     */
    private String type;
    /**
     * 是否自增
     */
    private Boolean autoIncrement;
    /**
     * 分表配置，mycat1.6之后开始支持，但dataNode 在分表条件下只能配置一个
     */
    private String subTables;
    /**
     * 与schema标签的sqlMaxLimit配合使用，如果needAddLimit值为false，则语句不会加上limit
     */
    private Boolean needAddLimit;
}
