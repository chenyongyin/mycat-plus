package com.cyy.pojo.schema;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: WriteHost
 * @ProjectName mycat-plus
 * @Description: writeHost 标签
 * @date 2019/8/7 17:17
 */
@Setter
@Getter
public final class WriteHost extends WriteOrReadHost{
    List<ReadHost> readHost;

    public WriteHost(String host, String url, String user, String password) {
        super(host, url, user, password);
    }
}
