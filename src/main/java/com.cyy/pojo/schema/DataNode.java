package com.cyy.pojo.schema;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: DataNode
 * @ProjectName mycat-plus
 * @Description: dataNode 标签
 * @date 2019/8/7 10:16
 */
@Getter
@RequiredArgsConstructor
public final class DataNode implements Serializable {
    /** 指定分片的名字*/
    private final String name;
    /** 定义该分片属于哪个数据库实例*/
    private final String dataHost;
    /** 定义该分片属于哪个具体数据库实例上的具体库（即对应mysql中实际的DB）*/
    private final String database;
}
