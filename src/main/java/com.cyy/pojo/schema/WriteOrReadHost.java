package com.cyy.pojo.schema;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: WriteOrReadHost
 * @ProjectName mycat-plus
 * @Description: writeHost 标签、 readHost 标签 基类
 * @date 2019/8/7 17:13
 */
@Setter
@Getter
@RequiredArgsConstructor
public class WriteOrReadHost {
    /** 用于标识不同实例，一般 writeHost 我们使用*M1，readHost 我们用*S1。**/
    private final String host;
    /** 后端实例连接地址，如果是使用 native 的 dbDriver，则一般为 address:port 这种形式。用 JDBC 或其他的dbDriver，则需要特殊指定。当使用 JDBC 时则可以这么写：jdbc:mysql://localhost:3306/。**/
    private final String url;
    /** 端存储实例需要的用户名字**/
    private final String user;
    /** 后端存储实例需要的密码**/
    private final String password;
    /** 权重 配置在 readhost 中作为读节点的权重（1.4 以后）**/
    private Integer weight;
    /** 是否对密码加密默认 0 否 如需要开启配置 1，同时使用加密程序对密码加密*/
    private Integer usingDecrypt;

}
