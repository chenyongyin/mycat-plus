package com.cyy.pojo.schema;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: DataHost
 * @ProjectName mycat-plus
 * @Description: dataHost标签
 * @date 2019/8/7 10:16
 */
@Setter
@Getter
@RequiredArgsConstructor
public class DataHost implements Serializable {
    /**
     * 指定dataHost的名字
     */
    private final String name;
    /**
     * 指定每个读写实例连接池的最大连接。也就是说，标签内嵌套的writeHost、 readHost 标签都会使用这个属
     * 性的值来实例化出连接池的最大连接数。
     */
    private int maxCon = 1000;
    /**
     * 指定每个读写实例连接池的最小连接，初始化连接池的大小。
     */
    private int minCon = 10;
    /**
     * 负载均衡类型：
     * （1）balance="0", 不开启读写分离机制，所有读操作都发送到当前可用的writeHost 上。
     * （2）balance="1"，全部的 readHost 与 stand by writeHost 参与 select 语句的负载均衡，简单的说，当双主双从模式(M1->S1，M2->S2，并且 M1 与 M2 互为主备)，正常情况下，M2,S1,S2 都参与 select 语句的负载均衡。
     * （3）balance="2"，所有读操作都随机的在 writeHost、 readhost 上分发。
     * （4）balance="3"，所有读请求随机的分发到 wiriterHost 对应的 readhost 执行，writerHost 不负担读压力，注意 balance=3 只在 1.4 及其以后版本有，1.3 没有。
     */
    private int balance = 0;
    /**
     * 1）writeType="0", 所有写操作发送到配置的第一个 writeHost，第一个挂了切到还生存的第二个riteHost，重新启动后已切换后的为准，切换记录在配置文件中:dnindex.properties.
     * （2）writeType="1"，所有写操作都随机的发送到配置的 writeHost，1.5 以后废弃不推荐。
     */
    private int writeType = 0;
    /**
     * 指定后端连接的数据库类型，目前支持二进制的 mysql 协议，还有其他使用 JDBC 连接的数据库。例如：mongodb、 oracle、 spark 等。
     */
    private final String dbType;
    /**
     * 指定连接后端数据库使用的 Driver，目前可选的值有 native 和 JDBC。使用native 的话，因为这个值执行的是二进制的 mysql 协议，所以可以使用 mysql 和 maridb。
     * 其他类型的数据库则需要使用 JDBC 驱动来支持。
     */
    private final String dbDriver;
    /**
     * -1 表示不自动切换
     * 1 默认值，自动切换
     * 2 基于 MySQL 主从同步的状态决定是否切换
     * 心跳语句为 show slave status
     */
    private int switchType = 1;
    /**
     * 如果配置了这个属性 writeHost 下面的 readHost 仍旧可用，默认 0 可配置（0、 1）。
     */
    private int tempReadHostAvailable = 0;
    /** 心跳检测*/
    private String heartbeat = "select user()";

    private final List<WriteHost> writeHost;
}
