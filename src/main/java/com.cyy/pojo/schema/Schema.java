package com.cyy.pojo.schema;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: Schema
 * @ProjectName mycat-plus
 * @Description: schema 标签配置
 * @date 2019/8/7 10:16
 */
@Setter
@Getter
@RequiredArgsConstructor
public final class Schema implements Serializable {
    /**
     * 配置逻辑库的名字（即数据库实例名）
     */
    private final String name;
    /**
     * 用于配置该逻辑库默认的分片。没有通过table标签配置的表，就会走到默认的分片上。
     * 这里注意没有配置在table标签的表，用工具查看是无法显示的，但是可以正常使用。
     * 如果没有配置dataNode属性，则没有配置在table标签的表，是无法使用的
     */
    private String dataNode;
    /**
     * 当前端执行【select *from USERDB.tf_user;】时（表名前指定了mycat逻辑库名），两种取值：
     * true：mycat会把语句转换为【select * from tf_user;】
     * false：会报错
     */
    private boolean checkSQLschema = true;
    /**
     * 相当于sql的结果集中，加上【limit N】。如果sql本身已经指定limit，则以sql指定的为准
     */
    private Integer sqlMaxLimit;
    /**
     * 逻辑库中的表
     */
    private List<Table> table;

}
