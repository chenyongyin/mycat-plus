package com.cyy.pojo.schema;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: readHost
 * @ProjectName mycat-plus
 * @Description: readHost 标签
 * @date 2019/8/7 17:17
 */
public final class ReadHost extends WriteOrReadHost{
    public ReadHost(String host, String url, String user, String password) {
        super(host, url, user, password);
    }
}
